package ca.landonjw.remoraids.api.commands.arguments.parsers;

import javax.annotation.Nonnull;
import java.util.Optional;

public class IntegerArgumentParser implements IArgumentParser<Integer> {

    @Override
    public Optional<Integer> parse(@Nonnull String argument) {
        try {
            int value = Integer.parseInt(argument);
            return Optional.of(value);
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

}